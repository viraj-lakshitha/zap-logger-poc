package utils

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"fmt"
	"os"
)

var Logger *zap.Logger

func InitializeLogger() {
	config := zap.NewProductionEncoderConfig()
	config.EncodeTime = zapcore.ISO8601TimeEncoder

	fileEncoder := zapcore.NewJSONEncoder(config)

	config.EncodeLevel = zapcore.CapitalColorLevelEncoder
	consoleEncoder := zapcore.NewConsoleEncoder(config)

	// Write Loggers to File
	// logFile, _ := os.OpenFile("trace.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	logWriter := zapcore.AddSync(&lumberjack.Logger{
		Filename: "onprem.log",
		MaxSize: 100, // in megabytes
		MaxBackups: 3,
		MaxAge: 30, // in days
	})

	// Debug Logger
	coreDebug := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, logWriter, zapcore.DebugLevel),
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), zapcore.DebugLevel),
	)
	Logger = zap.New(coreDebug, zap.AddCaller())
	defer func(logger *zap.Logger) { // flush buffer
		err := logger.Sync()
		if err != nil {
			fmt.Println("ZapLogger Error: ", err.Error())
		}
	}(Logger) // flush buffers
}
