package main

import (
	"zap-logger-poc/utils"
)

func main() {
	// Initialize Logger
	utils.InitializeLogger()

	utils.Logger.Info("First Debugger Message")
	utils.Logger.Error("First Error Message")
}
